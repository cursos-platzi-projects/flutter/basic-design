import 'package:basic_design/screens/home_screen.dart';
import 'package:basic_design/screens/scroll_design.dart';
import 'package:flutter/material.dart';

import 'package:basic_design/screens/basic_design.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    //Para darle otro tipo de color
    //SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith());

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'home_screen',
      routes: {
        'basic_design': ( _ ) => BasicDesignScreen(),
        'scroll_screen': ( _ ) => ScrollScreen(),
        'home_screen': ( _ ) => HomeScreen(),
      },
    );
  }
}



